package com.stonks.rsslistener.domain.rss

import com.stonks.rsslistener.core.Platform

trait FeedService[F[_]] {
  def process(feedUrl: String, platform: Platform, tag: Option[String]): F[Unit]
}
