package com.stonks.rsslistener

import cats.syntax.functor._
import cats.effect.ExitCode
import monix.eval.{Task, TaskApp}

object Main extends TaskApp {
  override def run(args: List[String]): Task[ExitCode] =
    Application.run[Task].as(ExitCode.Success)
}
