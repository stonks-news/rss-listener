package com.stonks.rsslistener.impl

import cats.{Functor, Monad}
import cats.effect.{Fiber, Timer}
import cats.syntax.flatMap._
import cats.syntax.functor._
import com.stonks.rsslistener.config.PlatformSchedulingConfig
import com.stonks.rsslistener.domain.rss.{FeedService, Worker}
import tofu.{Handle, Start}
import tofu.syntax.handle._
import tofu.logging._
import tofu.syntax.logging._

class WorkerImpl[F[_]: Monad: Start: Timer: Logging](implicit service: FeedService[F], handle: Handle[F, Throwable]) extends Worker[F] {
  override def start(info: PlatformSchedulingConfig): F[Fiber[F, Unit]] =
    info"Starting processing $info" >>
      Start[F].start(Timer[F].sleep(info.initialDelay) >> action(info))

  private def action(info: PlatformSchedulingConfig): F[Unit] =
    info"Tick ${info.platform}" >>
      (service
        .process(info.feedUrl, info.platform, info.tag)
        .handleWith { th: Throwable => error"Processing failed ${th.toString}" } >> (Timer[F].sleep(info.intervalDelay) >> action(info)))

}

object WorkerImpl {
  def apply[I[_]: Functor,
            F[_]: Monad: Start: Timer](implicit
                                       service: FeedService[F],
                                       handle: Handle[F, Throwable],
                                       logs: Logs[I, F]): I[Worker[F]] =
    logs.forService[Worker[F]].map(implicit l => new WorkerImpl)
}
