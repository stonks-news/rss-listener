package com.stonks.rsslistener.domain.rss

trait FeedClient[F[_]] {
  def get(url: String): F[Option[Feed]]
}
