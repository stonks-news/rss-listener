package com.stonks.rsslistener.core

import cats.Show
import cats.instances.string._
import doobie.util.{Get, Put}
import pureconfig.ConfigReader
import tofu.logging.Loggable


sealed abstract class Platform(val name: String)

object Platform {
  case object BBC extends Platform("bbc")
  case object Reuters extends Platform("reuters")

  val values: Seq[Platform] = Seq(BBC, Reuters)

  def find(name: String): Option[Platform] = values.find(_.name == name)

  // Instances

  implicit val platformConfigReader: ConfigReader[Platform] = ConfigReader.fromStringOpt(find)
  implicit val showPlatform: Show[Platform] = _.name
  implicit val loggablePlatform: Loggable[Platform] = Loggable.show

  implicit val platformDoobieGet: Get[Platform] = Get[String].temap(s => find(s).toRight("Fuck"))
  implicit val platformDoobiePut: Put[Platform] = Put[String].contramap(_.name)
}
