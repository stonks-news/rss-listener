package com.stonks.rsslistener

import cats.effect.{Async, Blocker, Bracket, Concurrent, ConcurrentEffect, ContextShift, Fiber, Resource, Sync, Timer}
import cats.implicits._
import cats.{Applicative, Monad, ~>}
import pureconfig.{ConfigReader, ConfigSource, Derivation}
import pureconfig.error.ConfigReaderException
import tofu.{Raise, Start}
import tofu.syntax.raise._
import com.stonks.rsslistener.config.{ApplicationConfig, DatabaseConfig, PlatformSchedulingConfig}
import com.stonks.rsslistener.domain.rss.{FeedClient, FeedService, Storage, Worker}
import com.stonks.rsslistener.impl.{ClickHouseStorage, FeedClientImpl, FeedServiceImpl, WorkerImpl}
import doobie.free.connection.ConnectionIO
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.middleware.FollowRedirect
import tofu.common.Console
import tofu.logging.Logs

object Application {
  import pureconfig.generic.auto._

  def parseConfig[F[_]: Sync, A](
    implicit
    raise: Raise[F, ConfigReaderException[A]],
    reader: Derivation[ConfigReader[A]]): F[A] = {
    Sync[F].delay(ConfigSource.default.load[A]) >>=
      (_.leftMap(ConfigReaderException.apply).toRaise[F])
  }

  implicit def connectionIOLift[F[_]](
    implicit xa: HikariTransactor[F],
    bracket: Bracket[F, Throwable]): ConnectionIO ~> F =
    xa.trans

  def transactor[F[_]: Async: ContextShift](config: DatabaseConfig): Resource[F, HikariTransactor[F]] =
    for {
      connectEc     <- ExecutionContexts.cachedThreadPool[F]
      transactionEc <- ExecutionContexts.cachedThreadPool[F]
      transactor <- HikariTransactor.newHikariTransactor[F](
        config.driver,
        s"jdbc:clickhouse://${config.host}:${config.port}/${config.database}",
        config.username,
        config.password,
        connectEc,
        Blocker.liftExecutionContext(transactionEc)
      )
    } yield transactor

  def httpClient[F[_]: ConcurrentEffect](): Resource[F, Client[F]] =
    for {
      connectEc <- ExecutionContexts.cachedThreadPool[F]
      client    <- BlazeClientBuilder[F](connectEc).resource
    } yield client


  def initApp[F[_]: Concurrent: Start: Timer](httpClient: Client[F], ta: HikariTransactor[F]): F[Worker[F]] = {
    implicit val _httpClient: Client[F] = FollowRedirect[F](maxRedirects = 5)(httpClient)
    implicit val _ta: HikariTransactor[F] = ta
    implicit val logs: Logs[F, F] = Logs.sync[F, F]

    implicit val storage: Storage[F] = ClickHouseStorage[F]
    implicit val feedClient: FeedClient[F] = FeedClientImpl[F]
    for {
      implicit0(feedService: FeedService[F]) <- FeedServiceImpl[F, F]
      implicit0(worker: Worker[F]) <- WorkerImpl[F, F]
    } yield worker
  }

  def setup[F[_]: ConcurrentEffect: ContextShift: Timer](conf: ApplicationConfig): Resource[F, Worker[F]] =
    for {
      client <- httpClient[F]
      ta <- transactor[F](conf.db)
      app <- Resource.liftF(initApp[F](client, ta))
    } yield app

  def runSources[F[_]: Monad](sources: List[PlatformSchedulingConfig], worker: Worker[F]): F[List[Fiber[F, Unit]]] =
    sources.traverse(worker.start)

  def run[F[_]: ConcurrentEffect: ContextShift: Timer]: F[Unit] =
    for {
      conf <- parseConfig[F, ApplicationConfig]
      workerRes = setup[F](conf)
      _ <- workerRes.use { worker =>
        runSources(conf.sources, worker) >>= waitForAll[F]
      }
    } yield ()

  def waitForAll[F[_]: Monad](fibers: List[Fiber[F, Unit]]): F[Unit] =
    fibers.foldLeft(Applicative[F].unit){ case (acc, f) => acc >> f.join }
}
