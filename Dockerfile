FROM mozilla/sbt:8u232_1.3.8 as build
# please keep the sbt version in line with the one in `project/build.properties` to avoid excessive downlaods

WORKDIR /work/
COPY build.sbt .
COPY project/ project/
RUN sbt update
COPY . .
RUN sbt stage


FROM openjdk:8

WORKDIR /opt/rss-listener/
COPY --from=build /work/target/universal/stage/bin/rss-listener ./bin/
COPY --from=build /work/target/universal/stage/lib ./lib

CMD ["/opt/rss-listener/bin/rss-listener"]
