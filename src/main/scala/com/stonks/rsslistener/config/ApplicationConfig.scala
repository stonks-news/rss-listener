package com.stonks.rsslistener.config

import com.stonks.rsslistener.core.Platform
import cats.syntax.semigroup._
import tofu.logging.{DictLoggable, LogRenderer, Loggable}

import scala.concurrent.duration.FiniteDuration

case class ApplicationConfig(
  db: DatabaseConfig,
  sources: List[PlatformSchedulingConfig]
)

case class DatabaseConfig(
  driver: String,
  host: String,
  port: Int,
  database: String,
  username: String,
  password: String
)

case class PlatformSchedulingConfig
(
  platform: Platform,
  tag: Option[String],
  feedUrl: String,
  initialDelay: FiniteDuration,
  intervalDelay: FiniteDuration
)

object PlatformSchedulingConfig {
  // Instances

  implicit val loggable: Loggable[PlatformSchedulingConfig] = new DictLoggable[PlatformSchedulingConfig] {
    override def fields[I, V, R, S](a: PlatformSchedulingConfig, i: I)(implicit r: LogRenderer[I, V, R, S]): R =
      r.addString("platform", a.platform.name, i) |+| r.addString("feed-url", a.feedUrl, i)

    override def logShow(a: PlatformSchedulingConfig): String =
      s"initial-delay=${a.initialDelay},interval-delay=${a.intervalDelay}"
  }
}
