package com.stonks.rsslistener.core.xml

import java.time.{LocalDateTime, ZonedDateTime}
import java.time.format.DateTimeFormatter

import cats.syntax.either._

import scala.xml.Node

trait XMLReads[A] {
  def readFrom(node: Node): Option[A]
}

object XMLReads {
  implicit val zonedDateTimeXMLReads: XMLReads[ZonedDateTime] = node =>
    Either.catchNonFatal(ZonedDateTime.parse(node.text, DateTimeFormatter.RFC_1123_DATE_TIME)).toOption

  implicit val localDateTimeXMLReads: XMLReads[LocalDateTime] = node =>
    Either.catchNonFatal(LocalDateTime.parse(node.text, DateTimeFormatter.RFC_1123_DATE_TIME)).toOption
}
