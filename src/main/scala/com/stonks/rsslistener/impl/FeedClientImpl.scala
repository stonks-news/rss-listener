package com.stonks.rsslistener.impl

import cats.effect.Sync
import cats.syntax.either._
import cats.syntax.option._
import cats.syntax.applicative._
import cats.syntax.functor._
import com.stonks.rsslistener.domain.rss.{Feed, FeedClient}
import com.stonks.rsslistener.core.xml.syntax._
import org.http4s.Status.Successful
import org.http4s.client.Client

import scala.xml.XML

class FeedClientImpl[F[_]: Sync](implicit client: Client[F]) extends FeedClient[F]{
  override def get(url: String): F[Option[Feed]] =
    client.get(url) {
      case Successful(resp) =>
        resp.bodyAsText.compile.string.map(parseResponse)
      case _ => none[Feed].pure[F]
    }

  // Internal

  def parseResponse(response: String): Option[Feed] =
    Either
      .catchNonFatal(XML.loadString(response))
      .toOption
      .flatMap(_.parse[Feed])
}

object FeedClientImpl {
  def apply[F[_]: Sync](implicit client: Client[F]): FeedClientImpl[F] = new FeedClientImpl
}
