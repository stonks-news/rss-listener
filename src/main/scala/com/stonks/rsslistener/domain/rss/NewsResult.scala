package com.stonks.rsslistener.domain.rss

import com.stonks.rsslistener.core.Platform

case class NewsResult(
  platform: Platform,
  tag: Option[String],
  feedEntry: FeedEntry,
  rawArticle: String,
  article: Option[Article]
)
