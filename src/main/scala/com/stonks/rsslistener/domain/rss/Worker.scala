package com.stonks.rsslistener.domain.rss

import cats.effect.Fiber
import com.stonks.rsslistener.config.PlatformSchedulingConfig


trait Worker[F[_]] {
  def start(info: PlatformSchedulingConfig): F[Fiber[F, Unit]]
}
