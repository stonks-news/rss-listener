package com.stonks.rsslistener.domain.parsers

import com.stonks.rsslistener.core.Platform
import com.stonks.rsslistener.domain.rss.Article

import scala.xml.Elem

trait PlatformParser[A <: Platform] {
  def parse(elem: Elem): Option[Article]
}
