package com.stonks.rsslistener.domain.rss

import java.sql.Timestamp
import java.time.LocalDateTime

import cats.Show
import cats.syntax.flatMap._
import cats.instances.option._
import com.stonks.rsslistener.core.xml.XMLReads
import com.stonks.rsslistener.core.xml.syntax._
import tofu.logging.Loggable

case class Feed(
  title: String,
  description: String,
  link: String,
  entries: List[FeedEntry]
)

object Feed {
  implicit val feedXmlReader: XMLReads[Feed] = root => {
    val node = root.asOpt("channel")
    for {
      title       <- node >>= (_.asOpt("title").asString)
      description <- node >>= (_.asOpt("description").asString)
      link        <- node >>= (_.asOpt("link").asString)
      entries     <- node >>= (_.asOpts("item"))
    } yield
      Feed(
        title,
        description,
        link,
        entries.flatParse[FeedEntry]
      )
  }

  implicit val feedShow: Show[Feed] = f => s"title=${f.title}, link=${f.link}"
  implicit val feedLoggable: Loggable[Feed] = Loggable.show
}

case class FeedEntry(
  title: Option[String],
  description: Option[String],
  link: String,
  guid: Option[String],
  author: Option[String],
  pubDate: Option[Timestamp],
)

object FeedEntry {
  implicit val feedXmlReader: XMLReads[FeedEntry] = node => {
    for {
      link <- node.asOpt("link")
    } yield
      FeedEntry(
        node.asOpt("title").asString,
        node.asOpt("description").asString,
        link.text,
        node.asOpt("guid").asString,
        node.asOpt("author").asString,
        node.asOpt("pubDate") >>= (_.parse[LocalDateTime].map(Timestamp.valueOf))
      )
  }

  implicit val feedShow: Show[FeedEntry] = f => s"title=${f.title}, link=${f.link}"
  implicit val feedLoggable: Loggable[FeedEntry] = Loggable.show
}
