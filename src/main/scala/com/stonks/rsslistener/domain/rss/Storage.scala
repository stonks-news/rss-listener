package com.stonks.rsslistener.domain.rss

trait Storage[F[_]] {
  def findEntry(url: String): F[Option[FeedEntry]]

  def insert(result: NewsResult): F[Int]
}


