CREATE DATABASE IF NOT EXISTS stonks;

CREATE TABLE IF NOT EXISTS stonks.news
(
    platform String,
    feed_title Nullable(String),
    feed_description Nullable(String),
    feed_link String,
    feed_guid Nullable(String),
    feed_author Nullable(String),
    feed_pub_date DateTime('Etc/GMT'),
    raw_article String,
    article_name Nullable(String),
    article_body Nullable(String)
)
ENGINE = MergeTree()
PRIMARY KEY feed_link
ORDER BY (feed_link, feed_pub_date)
;

ALTER TABLE stonks.news ADD COLUMN IF NOT EXISTS tag Nullable(String) DEFAULT NULL AFTER platform;
