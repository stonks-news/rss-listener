package com.stonks.rsslistener.domain

import cats.syntax.option._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import com.stonks.rsslistener.core.xml.syntax._
import com.stonks.rsslistener.domain.rss.{Feed, FeedEntry}

class FeedSpec extends AnyFlatSpec with Matchers {
  "XMLReads" should "parse correct feed" in {
    val xml =
        <rss version="2.0">
          <channel>
            <title>W3Schools Home Page</title>
            <link>https://www.w3schools.com</link>
            <description>Free web building tutorials</description>
            <item>
              <title>RSS Tutorial</title>
              <link>https://www.w3schools.com/xml/xml_rss.asp</link>
              <description>New RSS tutorial on W3Schools</description>
            </item>
            <item>
              <title>XML Tutorial</title>
              <link>https://www.w3schools.com/xml</link>
              <description>New XML tutorial on W3Schools</description>
            </item>
          </channel>
        </rss>

    xml.parse[Feed] shouldBe
      Feed(
        title = "W3Schools Home Page",
        link = "https://www.w3schools.com",
        description = "Free web building tutorials",
        entries = List(
          FeedEntry(
            title = "RSS Tutorial".some,
            description = "New RSS tutorial on W3Schools".some,
            link = "https://www.w3schools.com/xml/xml_rss.asp",
            author = None,
            pubDate = None,
            guid = None
          ),
          FeedEntry(
            title = "XML Tutorial".some,
            description = "New XML tutorial on W3Schools".some,
            link = "https://www.w3schools.com/xml",
            author = None,
            pubDate = None,
            guid = None
          )
        )
      ).some
  }
}
