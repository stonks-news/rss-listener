# RSS Listener

## Building

Constructing Docker Image:
```bash
sbt docker:publisLocal
```

Will output image: `stonks/rss-listener:<version>`

## Local Running
```bash
docker-compose up -d
```

Sets up local clickhouse database for experiments
