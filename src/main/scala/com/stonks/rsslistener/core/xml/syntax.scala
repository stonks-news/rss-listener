package com.stonks.rsslistener.core.xml

import cats.syntax.either._
import scala.xml.{Node, NodeSeq}

object syntax {
  implicit class XMLReadsNodeOps(val node: Node) extends AnyVal {
    def parse[A](implicit reader: XMLReads[A]): Option[A] =
      reader.readFrom(node)
  }

  implicit class XMLReadsNodeSeqOps(val node: NodeSeq) extends AnyVal {
    def parse[A](implicit reader: XMLReads[A]): List[Option[A]] =
      node.toList.map(_.parse[A])

    def flatParse[A](implicit reader: XMLReads[A]): List[A] =
      parse[A].flatten
  }

  implicit class NodeOps(val node: Node) extends AnyVal {
    def asOpt(key: String): Option[Node] =
      asOpts(key).flatMap(_.headOption)

    def asOpts(key: String): Option[NodeSeq] =
      Either.catchNonFatal(node \ key).toOption

    def asOptDeep(key: String): Option[NodeSeq] =
      asOptsDeep(key).flatMap(_.headOption)

    def asOptsDeep(key: String): Option[NodeSeq] =
      Either.catchNonFatal(node \\ key).toOption

  }

  implicit class NodeSeqOps(val node: NodeSeq) extends AnyVal {
    def asOpt(key: String): Option[NodeSeq] =
      Either.catchNonFatal(node \ key).toOption

    def asOptDeep(key: String): Option[NodeSeq] =
      Either.catchNonFatal(node \\ key).toOption
  }

  implicit class OptionNodeOps(val node: Option[Node]) extends AnyVal {
    def asString: Option[String] =
      node.map(_.text).filter(_.nonEmpty)
  }

  implicit class OptionNodeSeqOps(val node: Option[NodeSeq]) extends AnyVal {
    def asString: Option[String] =
      node.map(_.text).filter(_.nonEmpty)
  }
}
