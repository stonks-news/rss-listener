package com.stonks.rsslistener.impl

import cats.~>
import com.stonks.rsslistener.domain.rss.{FeedEntry, NewsResult, Storage}
import doobie._
import doobie.implicits._
import doobie.implicits.javasql._

class ClickHouseStorage[F[_]](implicit connectionIOLift: ConnectionIO ~> F) extends Storage[F] {
  override def findEntry(url: String): F[Option[FeedEntry]] =
    connectionIOLift(
      sql"""SELECT feed_title, feed_description, feed_link,
           |       feed_guid, feed_author, feed_pub_date
           |  FROM stonks.news
           | WHERE feed_link = $url
           |""".stripMargin.query[FeedEntry].option)

  override def insert(result: NewsResult): F[Int] = {
    val platform = result.platform
    val tag = result.tag
    val (feedTitle, feedDescription) = (result.feedEntry.title, result.feedEntry.description)
    val (feedLink, feedGuid) = (result.feedEntry.link, result.feedEntry.guid)
    val (feedAuthor, feedPubDate) = (result.feedEntry.author, result.feedEntry.pubDate)
    val rawArticle = result.rawArticle
    val (articleName, articleBody) = (result.article.map(_.name), result.article.map(_.body))

    connectionIOLift(sql"""INSERT INTO stonks.news(platform, tag, feed_title, feed_description, feed_link,
                          |                        feed_guid, feed_author, feed_pub_date, raw_article,
                          |                        article_name, article_body) 
                          |                VALUES ($platform, $tag, $feedTitle, $feedDescription, $feedLink,
                          |                        $feedGuid, $feedAuthor, $feedPubDate, $rawArticle,
                          |                        $articleName, $articleBody)
                          |""".stripMargin.update.run)
  }
}

object ClickHouseStorage {
  def apply[F[_]](implicit connectionIOLift: ConnectionIO ~> F): ClickHouseStorage[F] = new ClickHouseStorage[F]
}
