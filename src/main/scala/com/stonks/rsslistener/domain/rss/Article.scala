package com.stonks.rsslistener.domain.rss

case class Article(
  name: String,
  body: String,
)
