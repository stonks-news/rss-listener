package com.stonks.rsslistener.domain

import java.sql.Timestamp
import java.time.{LocalDate, LocalTime, ZoneId, ZonedDateTime}

import cats.syntax.option._
import org.scalatest.flatspec.AnyFlatSpec
import com.stonks.rsslistener.core.xml.syntax._
import com.stonks.rsslistener.domain.rss.FeedEntry
import org.scalatest.matchers.should.Matchers

class FeedEntrySpec extends AnyFlatSpec with Matchers {
  "XMLReads" should "parse correct feed entry" in {
    val xml =
      <item>
        <title>RSS Tutorial</title>
        <link>https://www.w3schools.com/xml/xml_rss.asp</link>
        <description>New RSS tutorial on W3Schools</description>
        <author>Nikita Vilunov</author>
        <pubDate>Mon, 16 Mar 2020 11:08:04 GMT</pubDate>
        <guid isPermaLink="true">https://www.w3schools.com/xml/xml_rss.asp</guid>
      </item>

    xml.parse[FeedEntry] shouldBe
      FeedEntry(
        title = "RSS Tutorial".some,
        description = "New RSS tutorial on W3Schools".some,
        link = "https://www.w3schools.com/xml/xml_rss.asp",
        author = "Nikita Vilunov".some,
        pubDate = Timestamp.from(ZonedDateTime.of(
          LocalDate.of(2020, 3, 16),
          LocalTime.of(11, 8, 4),
          ZoneId.of("Z")
        ).toInstant).some,
        guid = "https://www.w3schools.com/xml/xml_rss.asp".some
      ).some
  }
}
