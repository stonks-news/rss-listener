package com.stonks.rsslistener.impl

import cats.Functor
import cats.data.OptionT
import cats.effect.Sync
import cats.syntax.option._
import cats.syntax.functor._
import cats.syntax.applicative._
import cats.syntax.traverse._
import cats.syntax.flatMap._
import cats.instances.list._
import com.stonks.rsslistener.core.Platform
import com.stonks.rsslistener.domain.rss.{FeedClient, FeedEntry, FeedService, NewsResult, Storage}
import org.http4s.Status.Successful
import org.http4s.client.Client
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

class FeedServiceImpl[F[_]: Sync: Logging](implicit
                                           feedClient: FeedClient[F],
                                           httpClient: Client[F],
                                           storage: Storage[F]) extends FeedService[F] {

  override def process(feedUrl: String, platform: Platform, tag: Option[String]): F[Unit] =
    OptionT(feedClient.get(feedUrl))
      .semiflatMap(feed => info"Successfully parsed $feed".as(feed))
      .semiflatMap(_.entries.traverse(processSingle(platform, tag, _)))
      .value
      .void

  private def processSingle(platform: Platform, tag: Option[String], feedEntry: FeedEntry): F[Unit] =
    OptionT(info"Starting processing $feedEntry" >> storage.findEntry(feedEntry.link)).void
      .orElse(
        OptionT(info"Fetching $feedEntry" >> httpClient.get(feedEntry.link) {
          case Successful(resp) => resp.bodyAsText.compile.string.map(_.some)
          case _ => none[String].pure[F]
        })
          .semiflatMap(article => info"Successfully got ${feedEntry.link}".as(article))
          .semiflatMap { rawArticle =>
          val newsResult = NewsResult(
            platform,
            tag,
            feedEntry,
            rawArticle,
            None
          )
          storage.insert(newsResult)
        }
          .void
      )
      .value
      .void
}

object FeedServiceImpl {
  def apply[I[_]: Functor, F[_]: Sync](implicit
                                       logs: Logs[I, F],
                                       feedClient: FeedClient[F],
                                       httpClient: Client[F],
                                       storage: Storage[F]): I[FeedService[F]] =
    logs.forService[FeedService[F]].map(implicit l => new FeedServiceImpl[F])
}
